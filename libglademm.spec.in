%define RELEASE 1
%define rel %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}

Summary: A C++ wrapper for libglade
Name: @PACKAGE@
Version: @VERSION@
Release: %rel
License: LGPL
Group: System Environment/Libraries
Packager: Eric Bourque <ericb@computer.org>
Source: ftp://download.sourceforge/net/gtkmm/%{name}-%{version}.tar.gz
URL: http://gtkmm.sourceforge.net
Prefix: /usr
BuildRoot: /var/tmp/%{name}-%{version}-buildroot
Requires: libglade

%description

This package provides a C++ interface for libglademm. It is a
subpackage of the GTKmm project.  The interface provides a convenient
interface for C++ programmers to create Gnome GUIs with GTK+'s
flexible object-oriented framework.

%package devel
Summary: Headers for developing programs that will use libglademm.
Group: Development/Libraries
Requires: %{name}, libglade-devel

%description devel

This package contains the headers that programmers will need to
develop applications which will use libglademm, part of GTKmm, the C++
interface to the GTK+.

%prep
%setup -q

%build
# ...hope this can be removed soon
%ifarch alpha
        ARCH_FLAGS="--host=alpha-linux-gnu"
%endif
                                                                                
# Needed for snapshot releases.
if [ ! -f configure ]; then
        CFLAGS="$RPM_OPT_FLAGS" ./autogen.sh $ARCH_FLAGS \
                --prefix=%{prefix} \
                --disable-static \
                --enable-shared --enable-docs
else
        CFLAGS="$RPM_OPT_FLAGS" ./configure  $ARCH_FLAGS \
                --prefix=%{prefix} \
                --disable-maintainer-mode \
                --disable-static \
                --enable-shared --enable-docs
fi

if [ "$SMP" != "" ]; then
  make -j$SMP "MAKE=make -j$SMP"
  make
else
  make
fi

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README
%{prefix}/lib/*.so.*

%files devel
%{prefix}/include/libglademm-2.0/libglademm.h
%{prefix}/include/libglademm-2.0/libglademm
%{prefix}/lib/libglademm-2.0
%{prefix}/lib/*.la
#%{prefix}/lib/*.a # uncomment for static libraries
%{prefix}/lib/*.so
%{prefix}/lib/pkgconfig/libglademm-2.0.pc


%changelog
* Thu Sep 25 2003 Eric Bourque <ericb@computer.org> 
- Initial build.


